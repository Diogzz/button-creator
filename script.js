const controles = document.getElementById('controles');
const cssText = document.querySelector('.css');
const button = document.querySelector('.btn');

controles.addEventListener('change', handleChange);

let handleStyle = {
    element: button,
    backgroundColor(value) {
        this.element.style.backgroundColor = value;
    },
    color(value) {
        this.element.style.color = value;
    },
    height(value) {
        this.element.style.height = value + 'px';
    },
    width(value) {
        this.element.style.width = value + 'px';
    },
    texto(value) {
        this.element.innerText = value;
    },
    border(value) {
        this.element.style.border = value;
    },
    borderRadius(value) {
        this.element.style.borderRadius = value + 'px';
    },
    fontFamily(value) {
        this.element.style.fontFamily = value;
    },
    fontFamily(value) {
        this.element.style.fontFamily = value;
    },
    fontSize(value) {
        this.element.style.fontSize = value + 'rem';
    },
}

function handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    handleStyle[name](value);
    showCSS();
}

function showCSS() {
    cssText.innerHTML = '<span>' + button.style.cssText.split('; ').join('</span><span>');
}